import express, { Express, Request, Response } from 'express';
import { countMoney, formatAmount, getPrice, products, sellProduct, sellProductResponse } from '../services/products';

export function productController(req: Request, res: Response) {
    res.send(products);
}

export function productSellController(req: Request, res: Response) {
    const moneySubmitted = countMoney(req.body.coins);
    const productPrice = getPrice(req.body.productName) * 100;
    const missingCoins = req.body.missingCoins;
    const transaction: sellProductResponse = sellProduct(req.body.productName, req.body.coins, missingCoins);

    if (transaction.sell) {
        const data = {
            message: `Here is your product ${(req.body.productName)}, Product price is ${formatAmount(productPrice)}, Money submitted is ${formatAmount(moneySubmitted)},`,
            sell: transaction.sell,
            change: transaction.change,
        };
        res.json(data);
    }
    else {
        const data = {
            message: `Not enough money, product is ${req.body.productName}, product price is ${formatAmount(productPrice)}, money submitted is ${formatAmount(moneySubmitted)},`,
            sell: transaction.sell,
            change: transaction.change,
        };
        res.json(data);
    }
}
