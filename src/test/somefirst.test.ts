import { getAvailableCoinTypes, getChangeCoins, getPrice, sellProduct } from "../services/products";

test('Should return list of coins for change', () => {
    expect(getChangeCoins(223).change).toBe('100 100 20 2 1');
});

test('Should get product price', () => {
    expect(getPrice('Mars')).toBe(1.99);
});


test('Should return true to sell product', () => {
    expect(sellProduct("Mars", "100 50 20 50 20 10")).toStrictEqual( { sell: true, change: "50 1"});
});


test('Should return false to sell product', () => {
    expect(sellProduct("Mars", "50 2 1")).toStrictEqual( { sell: false, change: "50 2 1"});
});


test('Should show available coin types', () => {
    expect(getAvailableCoinTypes("50 1")).toBe('100 20 10 5 2');
});


test('Should return false if change is not available', () => {
    expect(sellProduct("Mars", "100 50 20 50 20 10", "1")).toStrictEqual( { sell: false, change: "100 50 50 20 20 10"});
});


test('Should change missing coins to available', () => {
    expect(sellProduct("Mars", "100 100 50", "50")).toStrictEqual( { sell: true, change: "20 20 10 1"});
});
