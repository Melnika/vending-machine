import exp from "constants";

export const coinTypes = [100, 50, 20, 10, 5, 2, 1];

export const products = {
  productNames: [
    { name: 'Mars', price: 1.99 },
    { name: 'Snickers', price: 1.85 },
    { name: 'Cola', price: 1.33 },
    { name: 'Salted nuts', price: 2.50 },
  ]
};

export interface productInterface {
  name?: string;
  price?: number;
}

export interface sellProductResponse {
  change: string;
  sell: boolean;
}

export interface getChangeAvailability {
  change: string;
  changeAvailability: boolean;
}

export function getPrice(productName: string): number {
  const product = products.productNames.find((product) => product.name === productName);
  if (product !== undefined) {
    return product.price
  } else {
    return 0;
  }
}

export function countMoney(coins: string) {
  const coinArray = coins.split(' ').map((coin: string) => parseInt(coin));
  const total = coinArray.reduce((a: number, b: number) => a + b, 0);
  return total;
}

export function sortDescending(coins: string): string {
  const coinArray = coins.split(' ').map(Number);
  coinArray.sort((a, b) => b - a);
  return coinArray.join(' ');
}

export function formatAmount(amount: number): string {
  return (amount / 100).toFixed(2);
}

export function getAvailableCoinTypes(coinsMissing: string): string {
  const coinArray = coinsMissing.split(' ').map(Number);
  const availableCoinTypes = coinTypes.filter((item) => !coinArray.includes(item));
  return availableCoinTypes.join(' ');
}

export function getNormalizedCoins(coinsUnavailable?: string): string {
  if (coinsUnavailable !== undefined) {
     return coinsUnavailable
  } else {
     return '';
  }
}

export function getChangeCoins(change: number, coinsUnavailable?: string): getChangeAvailability {
  const coinsMissing = getNormalizedCoins(coinsUnavailable);
  const availableCoinTypes = (getAvailableCoinTypes(coinsMissing)).split(' ').map(Number);
  const changeCoins = [];
  let changeLeft = change;
  for (let i = 0; i < availableCoinTypes.length; i++) {
    const coinType = availableCoinTypes[i];
    while (changeLeft >= coinType) {
      changeCoins.push(coinType);
      changeLeft = changeLeft - coinType;
    }
  }

  if (changeLeft > 0) {
    const data = {
      change: changeCoins.join(' '),
      changeAvailability: false,
    }
    return data;
  }
  else {
    const data = {
      change: changeCoins.join(' '),
      changeAvailability: true,
    }
    return data;
  }
}


export function sellProduct(productName: string, coinsSubmitted: string, coinsUnavailable?: string): sellProductResponse {
  const moneySubmitted = countMoney(coinsSubmitted);
  const productPrice = getPrice(productName) * 100;
  const coinsMissing = getNormalizedCoins(coinsUnavailable);
  if (moneySubmitted < productPrice) {
    const changeAmount = sortDescending(coinsSubmitted);
    return {
      change: changeAmount,
      sell: false,
    };
  }
  else {
    const changeAmount = getChangeCoins(moneySubmitted - productPrice, coinsMissing);
    if (changeAmount.changeAvailability === true) {
      return {
        change: changeAmount.change,
        sell: true,
      };
    }
    else
      return {
        change: sortDescending(coinsSubmitted),
        sell: false,
      };
  };
}
