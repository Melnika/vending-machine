import express, { Express } from 'express';
import dotenv from 'dotenv';
import { productController, productSellController } from './controllers/products';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

// eslint-disable-next-line @typescript-eslint/no-var-requires
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
// in latest body-parser use like below. 
app.use(bodyParser.urlencoded({ extended: true }));


app.get('/', productController);

app.post('/', productSellController);


app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});

